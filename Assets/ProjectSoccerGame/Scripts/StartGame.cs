﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Vuforia
{
    public class StartGame : MonoBehaviour
    {
        public InputField gamer1;
        public InputField gamer2;

        private void Start()
        {
                        
        }

        public void saveGamers() {
            NamesOfGamers namesGamers = new NamesOfGamers();
            namesGamers.nameGamers.Add(string.IsNullOrEmpty(gamer1.text) ? "Jugador 1" : gamer1.text);
            namesGamers.nameGamers.Add(string.IsNullOrEmpty(gamer2.text) ? "Jugador 2" : gamer2.text);
            PlayerPrefs.SetString("nameGamers", JsonUtility.ToJson(namesGamers));
            Debug.Log("Gamers: " + PlayerPrefs.GetString("nameGamers"));           
            
        }// fin de la funcion guardar jugadores

    }// fin de la clase
}