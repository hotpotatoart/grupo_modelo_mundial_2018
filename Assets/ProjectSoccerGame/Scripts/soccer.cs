﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vuforia
{
    public class soccer : MonoBehaviour
    {
        public GameObject porteria;
        public GameObject portero;
        public GameObject balon;
        float difL = 0.0f;
        public int goles;
        public GameObject goal;
        public List<TrackableBehaviour> trakersFounds = new List<TrackableBehaviour>();

        GameObject aux;
        GameObject aux2;

        private void Start()
        {
            
        }

        private void Update()
        {
           
        }

        IEnumerator waitTime(float time) {
            yield return new WaitForSeconds(time);
        }// fin del metodo esperar

        public void callingFoundTraker(TrackableBehaviour nameTraker) {
           if (this.trakersFounds.FindIndex(x => x == nameTraker) == -1)
            {
                Debug.Log("Se encontro: " + nameTraker.name);
                this.trakersFounds.Add(nameTraker);               
            }
            if (findTracker("LataPorteria") && findTracker("LataPorteria2"))
            {
                crearBalonYPorteria(nameTraker);
            }
            if (findTracker("LataPortero"))
            {
                crearPortero(nameTraker);
            }
        }// fin de la funcion 

        public void callingLostTraker(TrackableBehaviour nameTraker)
        {           
            this.trakersFounds.Remove(nameTraker);
            Debug.Log("Se perdio: " + nameTraker.name + ", " + this.trakersFounds.Count);
            if (!findTracker("LataPorteria") || !findTracker("LataPorteria2"))
            {
                borrarBalonYPorteria();
                GameObject.DestroyImmediate(aux);
                GameObject.DestroyImmediate(aux2);
            }            
            if (!findTracker("LataPortero"))
            {                
                borrarPortero();
            }        
        }// fin de la funcion

        public void crearBalonYPorteria(TrackableBehaviour trakerFound) {
            difL = trakersFounds[trakersFounds.FindIndex(x => x.name.Equals("LataPorteria2"))].transform.position.z / 2;
            goal.transform.localPosition = new Vector3(goal.transform.localPosition.x, goal.transform.localPosition.y, difL);
            if (!existPorteria())
            {
                aux = GameObject.Instantiate(porteria, GameObject.Find("FunctionManager").transform);   
                aux.transform.localPosition = new Vector3(4f, 10f, difL);
                aux.transform.localEulerAngles = new Vector3(0, 180f, 0);                
            }
            if (!existBall())
            {
                aux2 = GameObject.Instantiate(balon);
                aux2.transform.localPosition = new Vector3(30f, 0f, difL);
            }
        }// fin de la funcion crear balon y portero

        public void crearPortero(TrackableBehaviour trakerFound) {
            if (!existPortero())
            {
                GameObject.Instantiate(portero, Vector3.zero, Quaternion.identity, trakerFound.transform).
                    transform.localPosition = new Vector3(0.5f, 0.2f, 0);
            }
        }// fin de la funcion crear portero

        public void borrarBalonYPorteria(){
            GameObject.DestroyImmediate(GameObject.Find("Ball(Clone)"));  
            GameObject.DestroyImmediate(GameObject.Find("PorteriaF(Clone)"));            
        }// fin de la funcion crear balon y portero

        public void borrarPortero(){
            GameObject.DestroyImmediate(GameObject.Find("Portero(Clone)"));           
        }// fin de la funcion crear portero

        public void resetPositionBall() {
            if (existBall())
                GameObject.DestroyImmediate(GameObject.Find("Ball(Clone)"));
            GameObject.Instantiate(balon, new Vector3(30f, 0f, difL), Quaternion.identity);
        }

        public bool existPorteria(){
            return GameObject.Find("PorteriaF(Clone)");           
        }// fin de la funcion existe porteria

        public bool existBall(){
            return GameObject.Find("Ball(Clone)");
        }// fin de la funcion existe porteria

        public bool existPortero(){
            return GameObject.Find("Portero(Clone)");                       
        }// fin de la funcion existe porteria

        public bool findTracker(string nameTraker) {
            foreach (TrackableBehaviour traker in this.trakersFounds)
            {
                if (traker.name.Equals(nameTraker))
                    return true;
            }
            return false;
        }// buscar traker

    }// fin de la clase
}
