﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vuforia
{ 
    public class TrowBall: MonoBehaviour {

        public float disparo;
        public float lados;
        public float altura;
    
        private void OnMouseDrag()   
        {
           tirar();
        }

        public void tirar()
        {        
            foreach (Touch touch in Input.touches)
            {           
                this.GetComponent<Rigidbody>().AddForce(new Vector3( -disparo,
                                                                     Mathf.Abs(touch.deltaPosition.y * altura),
                                                                     -touch.deltaPosition.x * lados),
                                                                     ForceMode.Acceleration
                                                                     );                                                                 
                break;
            }
        }// fin de la funcoon tirar

        public void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.name.Equals("plano"))
            {
                GameObject.Find("FunctionManager").GetComponent<soccer>().goles++;
                Debug.Log("Gooooolll " + GameObject.Find("FunctionManager").GetComponent<soccer>().goles);
            }
        }// fin de la funcion Colisión

        IEnumerator waitTime(float time)
        {
            yield return new WaitForSeconds(time);
        }// fin del metodo esperar

            public static float ConvertPxToDP(float pixel)
        {
            float dpi = Screen.dpi == 0 ? 160.0f : Screen.dpi;
            float dp = pixel / (dpi / 160.0f);
            return dp;
        }// fin de la funcion

    }
}