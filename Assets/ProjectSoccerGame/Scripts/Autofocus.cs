﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
public class Autofocus : MonoBehaviour {

    private void Start()
    {
        VuforiaARController.Instance.RegisterVuforiaStartedCallback(() =>{
            CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        });
    }
}
